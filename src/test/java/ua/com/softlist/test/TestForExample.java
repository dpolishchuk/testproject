package ua.com.softlist.test;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestForExample {

    @Test
    public void someTest(){
        Logic logic = new Logic();
        int expected = 2;
        int real = logic.divide(8.0,4.0).intValue();
        assertEquals(expected, real);
    }
}
