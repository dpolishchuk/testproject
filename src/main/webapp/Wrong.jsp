<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>You are welcome!</title>
    <style>
        .fig {
            text-align: center;
            margin-top: 115px;
        }
        .fig img{
            height: 220px;
        }
        .textt {
            text-align: center;
            margin-top: 50px;
            color: #ca6354;
        }
    </style>
</head>
<body>

    <div class="fig">
        <img src="/img/wrong.jpg" alt="=(">
    </div>
    <div class="textt">
        <h1>It's Ok! Not all are lucky</h1>
    </div>

</body>
</html>
