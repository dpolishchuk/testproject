<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>You are welcome!</title>
    <style>
        .fig {
            text-align: center;
            margin-top: 115px;
        }
        .fig img{
            height: 220px;
        }
        .textt {
            text-align: center;
            margin-top: 50px;
            color: #69caa4;
        }
    </style>
</head>
<body>

    <div class="fig">
        <img src="/img/congratulation.jpg" alt="=)">
    </div>
    <div class="textt">
       <h1>And you are lucky!</h1>
    </div>

</body>
</html>
