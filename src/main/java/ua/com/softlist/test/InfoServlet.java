package ua.com.softlist.test;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class InfoServlet  extends HttpServlet {

    private final String PARAM = "answer";
    private final String RIGHTANSWER = "82";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        RequestDispatcher dispatcher;
        if(request.getParameter(PARAM).equalsIgnoreCase(RIGHTANSWER)){
            dispatcher = request.getRequestDispatcher("/Right.jsp");
        }else{
            dispatcher = request.getRequestDispatcher("/Wrong.jsp");
        }
        dispatcher.forward(request, response);

    }
}
